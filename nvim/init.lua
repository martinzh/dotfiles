-- disable netrw at the very start of your init.lua
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

-- Define leader key
vim.g.mapleader = '<space>'

-- Agrega numero de lineas
vim.opt.number = true

-- Activa numero relativo de lineas
vim.wo.relativenumber = true

vim.opt.smartcase = true

-- Numero de espacios para indentar
vim.opt.tabstop = 2
vim.opt.shiftwidth = 2

-- Transforma tabs a espacios
vim.opt.expandtab = true

local lazy = {}

function lazy.install(path)
  if not vim.loop.fs_stat(path) then
    print('Installing lazy.nvim...')
    vim.fn.system({
      "git",
      "clone",
      "--filter=blob:none",
      "https://github.com/folke/lazy.nvim.git",
      "--branch=stable", -- latest stable release
      path,
    })
  end
end

function lazy.setup(plugins)
  -- Esta linea solo se usa para instalar lazy
  -- lazy.install(lazy.path)
  vim.opt.rtp:prepend(lazy.path)
  require("lazy").setup(plugins, lazy.opts)
end

lazy.path = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
lazy.opts = {}

lazy.setup({
  {'folke/tokyonight.nvim'},
  {'tpope/vim-repeat'},
  {'lukas-reineke/indent-blankline.nvim'},
  {'ggandor/leap.nvim'},
  {'ggandor/flit.nvim'},
  {'kylechui/nvim-surround',
  version = '*', -- Use for stability; omit to use `main` branch for the latest features
  event = 'VeryLazy',
},
{'numToStr/Comment.nvim',
opts = {
  -- add any options here
},
lazy = false,
  },
  {'nvim-lualine/lualine.nvim'},
  {'nvim-tree/nvim-web-devicons'},
  {'nvim-tree/nvim-tree.lua'},
  {'nvim-treesitter/nvim-treesitter'},
})

-- Colorscheme
vim.opt.termguicolors  = true
vim.cmd.colorscheme('tokyonight')

-- Inicializa nvim-tree using defaults
require("nvim-tree").setup()

-- Inicializa lualine
require('lualine').setup()
--{
--tabline = {
--  lualine_a = {'buffers'},
--  lualine_b = {'branch'},
--  lualine_c = {'filename'},
--  lualine_x = {},
--  lualine_y = {},
--  lualine_z = {'tabs'}
--}

-- Inicializa Leap.nvim
require('leap').add_default_mappings()

-- Inicializa flit.nvim
require('flit').setup()

-- Incializa nvim-surround
require('nvim-surround').setup({
  -- Configuration here, or leave empty to use defaults
})

-- Inicializa Comment.nvim
require('Comment').setup()

-- Indent guides
vim.opt.list = true
-- vim.opt.listchars:append "space:⋅"
-- vim.opt.listchars:append "eol:↴"

require("indent_blankline").setup {
  space_char_blankline = " ",
  show_current_context = true,
  show_current_context_start = false,
}

-- NvimTree config
local api = require("nvim-tree.api")

local function edit_or_open()
  local node = api.tree.get_node_under_cursor()

  if node.nodes ~= nil then
    -- expand or collapse folder
    api.node.open.edit()
  else
    -- open file
    api.node.open.edit()
    -- Close the tree if file was opened
    api.tree.close()
  end
end

-- open as vsplit on current node
local function vsplit_preview()
  local node = api.tree.get_node_under_cursor()

  if node.nodes ~= nil then
    -- expand or collapse folder
    api.node.open.edit()
  else
    -- open file as vsplit
    api.node.open.vertical()
  end

  -- Finally refocus on tree if it was lost
  api.tree.focus()
end

-- global
vim.api.nvim_set_keymap("n", "<C-h>", ":NvimTreeToggle<cr>", {silent = true, noremap = true})

-- on_attach
-- vim.keymap.set("n", "l", edit_or_open,          opts("Edit Or Open"))
-- vim.keymap.set("n", "L", vsplit_preview,        opts("Vsplit Preview"))
-- vim.keymap.set("n", "h", api.tree.close,        opts("Close"))
-- vim.keymap.set("n", "H", api.tree.collapse_all, opts("Collapse All"))

vim.keymap.set("n", "l", edit_or_open,          {})
vim.keymap.set("n", "L", vsplit_preview,        {})
vim.keymap.set("n", "h", api.tree.close,        {})
vim.keymap.set("n", "H", api.tree.collapse_all, {})

