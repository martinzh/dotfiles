local M = {}

M.mzh ={
  n = {
    -- Slime Cells
    -- ["<leader>jn"] = {"<Plug>SlimeCellsNext<CR>", "Slime Next Cell"},
    -- ["<leader>jp"] = {"<Plug>SlimeCellsPrev<CR>", "Slime Prevoius Cell"},
    -- ["<leader>jc"] = {"<Plug>SlimeCellsSendAndGoToNext<CR>", "Slime Send Cell"},
    ["<leader>jn"] = {":IPythonCellNextCell<CR>", "IPythonCell Next"},
    ["<leader>jp"] = {":IPythonCellPrevCell<CR>", "IPythonCell Previous"},
    ["<leader>jc"] = {":IPythonCellExecuteCellVerbose<CR>", "IPythonCell Execute Cell"},
    ["<leader>jl"] = {":IPythonCellClear<CR>", "IPythonCell Clear"},
    ["<leader>ja"] = {":IPythonCellInsertAbove<CR>", "IPythonCell Insert Cell Above"},
    ["<leader>jb"] = {":IPythonCellInsertBelow<CR>", "IPythonCell Insert Cell Below"},
    ["<leader>js"] = {":SlimeSendCurrentLine<CR>", "Slime Send Current Line"},
    -- VimTex
    ["<leader>lc"] = {":VimtexCompile<CR>", "Start VimTex Compiler"},
    ["<leader>lv"] = {":VimtexView<CR>", "View generated PDF"},
    ["<leader>lo"] = {":VimtexTocOpen<CR>", "Open document's TOC"},
    ["<leader>lt"] = {":VimtexTocToggle<CR>", "Toggle document's TOC"},
  },
  x = {
    ["<leader>js"] = {"<Plug>SlimeRegionSend<CR>", "Slime Send Selection"},
  }
}

return M
