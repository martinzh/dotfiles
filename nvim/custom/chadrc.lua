---@type ChadrcConfig
local M = {}

M.ui = {
  theme = 'chadracula',
  statusline = {
    separator_style = "round"
  }
}

vim.api.nvim_command('filetype plugin indent on')
vim.api.nvim_command('syntax on')

vim.g.python3_host_prog = "/home/martinzh/code_envs/py_envs/nvim_env/bin/python"

vim.wo.relativenumber = true
vim.o.foldmethod = "marker"

M.plugins = "custom.plugins"
M.mappings = require "custom.mappings"

return M
