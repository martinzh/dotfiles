local plugins = {
  {
    "ggandor/leap.nvim",
    init = function()
      require('leap').add_default_mappings()
    end
  },
  {
    "ggandor/flit.nvim",
    dependencies = {
      "ggandor/leap.nvim",
      "tpope/vim-repeat"
    },
    init = function()
      require("flit").setup()
    end
  },
  {
    "kylechui/nvim-surround",
    init = function ()
      require("nvim-surround").setup({})
    end
  },
  {
    "hanschen/vim-ipython-cell",
    lazy = true,
    ft = {"python"},
    -- ft = {"python", "julia"},
    dependencies = {"jpalardy/vim-slime"},
    init = function ()
      vim.g.loaded_python3_provider = nil
      vim.g.python3_host_prog = "/home/martinzh/code_envs/py_envs/nvim_env/bin/python"
      -- vim.g.slime_target = "neovim"
      vim.g.slime_target = "kitty"
      vim.g.ipython_cell_delimit_cells_by = "tags"
      vim.g.slime_cell_delimiter = "##"
      -- vim.g.slime_cell_delimiter = "# %%"
      -- vim.g.slime_cell_delimiter = "\\s*##"
      vim.g.slime_python_ipython = 1
    end
  },
  -- {
  --   "klafyvel/vim-slime-cells",
  --   lazy = true,
  --   ft = {"julia"},
  --   -- ft = {"python", "julia"},
  --   dependencies = {"jpalardy/vim-slime"},
  --   init = function ()
  --     -- vim.g.slime_target = "neovim"
  --     vim.g.slime_target = "kitty"
  --     vim.g.slime_cell_delimiter = "^\\s*##"
  --     vim.g.slime_bracketed_paste = 1
  --     vim.g.slime_no_mappings = 1
  --   end
  -- },
  {
    "lervag/vimtex",
    ft = {"tex", "plaintex"},
    init = function()
      -- vim.g.vimtex_view_method = "sioyek"
      vim.g.vimtex_view_method = "zathura"
      vim.g.vimtex_compiler_latexmk = {
        aux_dir = "aux",
        out_dir = "build"
      }
    end
  }
}

return plugins
